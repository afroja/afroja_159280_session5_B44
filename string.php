<?php

$var=100;
$value1 = 'my string:$var';
$value2 = "my string:$var";

echo $value1."<br>";
echo $value2;

$heredoc=<<<BITM

 this is heredoc line 3 $var
 this is heredoc line 14 $var

BITM;
echo "<br><br>";
echo $heredoc;

$nowdoc=<<<'BITM'

 this is nowdoc line 3 $var
 this is nowdoc line 14 $var

BITM;
echo "<br><br>";
echo $nowdoc;


?>